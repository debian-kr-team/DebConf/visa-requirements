# South Korean Detailed Visa requirements case by case

Do you need C-3-1 Visa to visit Korea and attend a meeting or conference? Required documents to apply for visa are defer by your status and the Korean diplomatic mission you will visit. This repo provides detailed information case by case.

## Index

| Country(or region) - click text for details | Accomodation reservation | Flight booking | Financial ability proof | Invitee ID or Status Certificate |
| --- | --- | --- | --- | --- |
| [Israel(Stateless or Refugees)](./visa-requirements/israel_stateless.md) | Required | Required | Bank statement, (If employeed)Certificate of the employment | Required |
| [Iran](./visa-requirements/iran.md) | - | - | Certificate of employment or enrollment | Required |
| [Kosovo](./visa-requirements/kosovo.md) | - | - | (If employeed)Certificate of the employment and Dispatch Order | - |
| [India(Northern)](./visa-requirements/india_northern.md) | - | - | Salary Slip, ITR, (If employeed)Certificate of the employment | - |
| [India(Southern)](./visa-requirements/india_southern.md) | - | - | Salary Slip, ITR, (If employeed)Certificate of the employment | - |
| [Nepal](./visa-requirements/nepal.md) | Yes | Yes | Job certificate, Firm registration, PAN registration, Bank statement | - |
| [Mainland China(Wuhan)](./visa-requirements/mainland_china_wuhan.md) | - | - | - | ID Card, Certificate of employment or enrollment |

## Visa process

https://storm.debian.net/shared/HWHh12nd4cHSn-6G0VOrcBvTXn3sJVNd0A490qv4W4M