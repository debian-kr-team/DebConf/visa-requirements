# Visitors with Indian passport coming from Southern India

> Source:
> - https://overseas.mofa.go.kr/in-ko/brd/m_2803/view.do?seq=1301843&page=1
> - https://www.visaforkorea-ce.com/assets/pdfs/business-2024.pdf
> - https://overseas.mofa.go.kr/in-chennai-ko/brd/m_2924/view.do?seq=1287122&page=1

If place of issue (on first page) or address (in last page) in your passport is one of following regions, you may prepare visa based on this page.

Union territory:
- Lakshadweep

States:
- Andhra Pradesh
- Kerala
- Karnataka
- Tamil Nadu

## Apply at 
Either one of these visa application center near from you. [Visit this website to apply](https://www.visaforkorea-ce.com/l)

- Chennai Visa Application Centre
  - Link to Google maps: https://maps.app.goo.gl/bFPEUPXGoA8gq8Z5A
  - Contact: +912267866040
  - Address: Korea Visa Application Center, Fagun Towers 2nd Floor, No 74, Ethiraj Salai, Egmore, Chennai - 600008 , India
- Bangalore Visa Application Centre
  - Link to Google maps: https://maps.app.goo.gl/kByktmLLwMdLSctG6
  - Address: 22, Gopalan Innovation Mall, Bannerghatta Main Rd, Sarakki Industrial Layout, 3rd Phase, J. P. Nagar,Bengaluru, Karnataka 560076, India

Details on website: https://www.visaforkorea-ce.com/attend-centre.html

## Invitee (Visitor) to prepare

> Based on this [checklist](https://www.visaforkorea-ce.com/assets/pdfs/business-2024.pdf) and [notice on consulate website](https://overseas.mofa.go.kr/in-chennai-ko/brd/m_2924/view.do?seq=1287122&page=1)

- Original Passport with more than 6 months' validity.
- Recently taken 2 passport sized colored photographs.
- Visa application form filled in with e-Form or Duly filled Visa application form by Applicant with Blue or Black ball pen in Block Letters.
- Occupation Proof or Employment Proof.
- 1 Year salary slip.
- Photocopy of Income Tax Return (ITR) of last 2 year. (If unable to submit any of ITR then a letter of explanation mentioning the reason must be Submitted).
- In case of ownership please submit 3 year company Income Tax Return (for proprietor please submit 3 year personal ITR ).
- Recent Bank Statement of last 6 months [original or photocopy with the bank's stamp and sign by bank Authority].
- Company registration copy.
- Business profile details in case of ownership in format only (Please refer download form section for format).
- Health Condition Form (Please refer to home page). 


## Inviter (Organizer) to prepare

> Note: Photocopy of documents (such as PDF) allowed to be submitted. 

- Invitation letter
- The business certificate of the company(사업자등록증 혹은 사업자등록증명원)
- Event brochure