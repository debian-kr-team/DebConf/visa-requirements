# Visitors with Mainland China passport with residency in Wuhan or nearby regions

If you're coming from Hubei Province(湖北省), Hunan Province(湖南省), Henan Province(河南省), Jiangxi Province(江西省) following applies to you.

> Source:
> - https://overseas.mofa.go.kr/cn-wuhan-ko/brd/m_1011/view.do?seq=1083995&page=1
> - https://overseas.mofa.go.kr/cn-wuhan-zh/brd/m_982/view.do?seq=710800&page=1

## Apply at 
Korea Visa Application Center in Wuhan 

- Website: https://visaforkorea-wh.com/
- Address: 武汉市江汉区建设大道588号卓尔国际中心15层1503
- Contact: 400-813-3666
- Email: kvac_wuhan@visaforkorea-wh.com

## Invitee (Visitor) to prepare
- Passport
- A Pasport Photo
- Visa application form filled in
- National ID Card (Both Orginal and copy)
- Certificate of employement or Certificate of enrollment (Original)
- A copy of the business license or a copy of the organization code certificate (the official stamp seal is required).
- In case, your residency in the region is temporary (due to study, work or other reasons) or your household is located outside of jurisdiction of Korean Consulate General in Wuhan Following documents are also required.
  - Valid residence permit (temporary residence permit) original and copy (issued more than two months)
  - Students enrolled in the area under the jurisdiction of the museum to submit the online school registration verification report of the school credit website.

## Inviter (Organizer) to prepare
- Invitation letter (Original)
  - Should include invitation details and return gurantee
- Copy of Business registration