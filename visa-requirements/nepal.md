# Visitors with Nepali Passport
> Source:
> - https://overseas.mofa.go.kr/np-ko/brd/m_1730/view.do?seq=1214566&page=1
> - https://overseas.mofa.go.kr/np-en/wpge/m_1714/contents.do

## Online booking required to submit Visa application on-site
The embassy receives your Visa application only if you've booked visit schedule in-advance. Visit following URL to book your schedule.
https://consul.mofa.go.kr/en/main.do

## Invitee (Visitor) to prepare

- Visa application form filled in(along with passport photo 1pc)
- Original and copy of passport
- Flight schedule booking confirmation
    - Usually e-Ticket of your round trip
    - Departure schedule should be 15 days after the day of Visa application received by the embassy
- Accomodation booking confirmation - Such as Hotel voucher
- Visa cover letter 
    - Cover letter that describes your purpose of visit to Korea, Travel funding details and more.
- Original or copy of documents that proves your occupation
    - Job certificate (or Certificate of employement) - For employees
    - Firm registration (or Business registration) - For Business owners
    - PAN registration
- Bank statement of last 6 months
- Firm bank statement of last 6 months    

## Inviter (Organizer) to prepare

> In principle, all documents must be submitted in the original

- Invitation letter - With Registered stamp of the legal entity sealed. If that's not possible, Notarization is required.
- Letter of gurantee - With Registered stamp of the legal entity sealed. If that's not possible, Notarization is required.
- The business certificate of the company(사업자등록증 혹은 사업자등록증명원)
- Event brochure, Event timatable

## Notes
These are most common reasons for rejecting Visa application
- Passport or travel document is not valid
- Those who subject to [Immigration Act Articla 11 Paragraph 1](https://www.law.go.kr/영문법령/출입국관리법/(19070,20221213))
- In the case of violating Korean laws, such as immigration-related laws in the past
- Failure to submit documents necessary for clarifying the purpose of entry
- Where it does not meet the requirements of the status of residence prescribed by the Immigration Control Act
- If the authenticity of the submitted document is not confirmed
- Failure to fully clarify the purpose of entry
- Where family relationships and economic conditions fail to clarify that they will return home within the scheduled period of stay
- Where the invitation qualification of the invited person is ineligible
- Where the relationship with the invited person is not proven
- If you fail the interview related to visa issuance
- Where false statements, submissions of false documents, false facts, etc. are entered in connection with the issuance of visas
- Where the issuance of other visas is deemed to be in violation of national interests

