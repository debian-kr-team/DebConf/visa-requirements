# Visitors with Kosovo Passport
> Source:  email inquiry with Embassy in Austria: "Nahyun Chung" <nhchung21@mofa.go.kr>

## Invitee (Visitor) to prepare

- Visa application form filled in
- passport photos (2 recommended)
- Original passport
- Copy of passport
- Application fee (to be updated later)
- If you are an employee:
    - Certificate of employement
    - Original Dispatch Order from your employer

## Inviter (Organizer) to prepare

> Note: Photocopy of documents (such as PDF) allowed to be submitted. But recommended to have registered stamped sealed with legal Stamp certificate attached or notarized.

- Invitation letter
- The business certificate of the company(사업자등록증 혹은 사업자등록증명원)
- (Recommended, Not a requirement) Letter of gurantee
- Event brochure

## How to apply via DHL
1. Prepare all documents
2. Applicant to contact Korean embassy in Austria
3. Officer will reply with application fee and wire transfer information
4. Transfer application fee to Korean embassy in Austria then keep the wire transfer receipt
5. Send all prepared documents with transfer receipt to Korean embassy in Austria via DHL
6. Once received by Korean embassy in Austria, they will contact to inform if all documents looks good or more documents required

## Notes
- It takes about 10 working days to get visa issued.
- As there are no Korean diplomatic mission in Kosovo, You will need to visit Korean embassy in Austria. OR You may apply for visa by sending all required documents via DHL to Korean embassy in Austria.


