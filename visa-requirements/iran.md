# Visitors with Iranian Passport

> Source(Korean): https://overseas.mofa.go.kr/ir-ko/brd/m_11392/view.do?seq=1284203&page=1
> Source(English): https://overseas.mofa.go.kr/ir-en/wpge/m_11373/contents.do

## Invitee (Visitor) to prepare

- Visa application form filled in
- 2 passport photos
- Application fee 50 USD (100 USD for express)
- Passport and photocopy of the first page of passport
- Photocopy of National ID and Birth Certificate (Page 1 & 2 / in English)
- For employees: Certificate of employement (required to be translated into English)
- For students: Certificate of enrollment (required to be translated into English)
- For visitors other then emplyees or students: Print of bank statement(last three months) and financial support letter of parents/or spouse with the copy of birth certificate of that person with English translation and notarization

## Inviter (Organizer) to prepare

> Note: Photocopy of documents (such as PDF) allowed to be submitted

- Invitation letter
 - Following specific text to gurantee about the invitee will return to Iran must be included in the letter
   - In Korean: `피초청자가 이란으로 돌아가지 않을 경우(불체발생), 초청자는 향후 초청시 보다 엄격한 심사를 감수한다.`
   - In English: `If the invitee will not return to Iran, the inviter understands that it will be subject to very severe review process for further invitation.`
- The business certificate of the company(사업자등록증 혹은 사업자등록증명원)
- In case of financial gurantee from the inviter, the embassy could require financial statements to the inviter

## Office hours of the Embassy in Iran 
Receiving application: Sun.-Thu. 8:30~11:30 / Pick-up: Sun.-Thu.14:00~15:30



 
