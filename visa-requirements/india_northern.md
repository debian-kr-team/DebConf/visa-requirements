# Visitors with Indian passport coming from Northern India

> Source:
> - https://overseas.mofa.go.kr/in-ko/brd/m_2803/view.do?seq=1301843&page=1
> - https://www.visaforkorea-id.com/assets/pdfs/SHORT%20TERM%20-%20BUSINESS%20MEETINGSCONFERENCEEVENTS.pdf
> - https://overseas.mofa.go.kr/in-en/wpge/m_2660/contents.do

If place of issue (on first page) or address (in last page) in your passport is one of following regions, you may prepare visa based on this page.

Union territory:
- National Capital Territory of Delhi
- Chandigarh
- Andaman and Nicobar Islands
- Jammu &Kashmir
- Ladakh

States:
- Arunachal Pradesh
- Assam
- Bihar
- Chhattisgarh
- Haryana
- Himachal Pradesh
- Jharkhand
- Rajastan
- Meghalaya
- Manipur
- Misoram
- Nagaland
- Orisha
- Punjab
- Sikkim
- Tripura
- Uttarakhand
- Uttar Pradesh
- West Bengal 

## Apply at 
Either one of these visa application center near from you. [Visit this website to apply](https://www.visaforkorea-id.com/index.html)

- New Delhi VFS Global Korean Visa Application Centre
  - Contact: +91-22-6201-8463
  - Address: VFS Korea Visa Application Centre, Shivaji Stadium Metro Station, Baba Kharak Singh Marg, Connaught Place, New Delhi - 110001
- Kolkata VFS Global Korean Visa Application Centre
    - Contact: +91-22-6201-8463
    - Address: 4th Floor, Rene Tower, Plot No. AA-I, 1842 Rajdanga Main Road, Kasba, Kolkata 700107. West Bengal. India. (Beside Acropolis Mall. Near Kasba New Market and Geetanjali Stadium)

## Invitee (Visitor) to prepare

> Based on this [checklist](https://www.visaforkorea-id.com/assets/pdfs/SHORT%20TERM%20-%20BUSINESS%20MEETINGSCONFERENCEEVENTS.pdf)

- Original Passport with more than 6 months' validity.
- Recently taken 2 passport sized colored photographs.
- Duly filled Visa application form by Applicant with Blue or Black ball pen in Block Letters.
- Occupation Proof or Employment Proof.
- 1 Year salary slip.
- Photocopy of Income Tax Return (ITR) of last 2 year. (If unable to submit any of ITR then a letter of explanation mentioning the reason must be Submitted).
- In case of ownership please submit 3 year company Income Tax Return (for proprietor please submit 3 year personal ITR ).
- Recent Bank Statement of last 6 months [original or photocopy with the bank's stamp and sign by bank Authority].
- Company registration copy.
- Business profile details in case of ownership in format only (Please refer download form section for format).
- Health Condition Form (Please refer to home page). 


## Inviter (Organizer) to prepare

> Note: Photocopy of documents (such as PDF) allowed to be submitted. 

- Invitation letter
- The business certificate of the company(사업자등록증 혹은 사업자등록증명원)
- Event brochure