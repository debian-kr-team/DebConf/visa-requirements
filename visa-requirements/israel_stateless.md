# Israel - Stateless or Refugees visitors with Israeli Provisional Passport(a.k.a. Lasse Passe) 

> Source: email inquiry with 주이스라엘국대사관 <israel@mofa.go.kr>

## Invitee (Visitor) to prepare

> Note: All the documents needed in English

- A valid passport with at least 6 months remainder validity (original and copy of bio-data page)
- A completed Visa application filled with the capital letters (Please see the attached file from the link: https://overseas.mofa.go.kr/il-en/brd/m_23223/view.do?seq=5&page=1)
  - **Note: Application form will not be given at the embassy**
- A recent passport photo (3.5 x 4.5cm, color photo with the white background)
- Fee: USD 40 IN CASH ONLY
- Recent 3 months history statements of Israel bank account (Credit/debit card statements will not be accepted)
- Accomodation reservation (ex. Hotel booking confirmation)
- If you are employed, the certificate of the employment (Or an official letter from your employer)
  - The document must be an original printout with handwritten signature and company stamp sealed. (Scanned copy is not allowed)
- Round trip flight ticket reservation
- Another passport original and copy if you have one.
- Schedule of stay (Please see the attached file from the link: https://overseas.mofa.go.kr/il-en/brd/m_23223/view.do?seq=7&page=1)
-  Official certificate of your status in Israel
- Israel criminal records with Apostille

## Inviter (Organizer) to prepare

> Original printout required for companies or organization. Scanned copy allowed for government organizations

- Invitation letter
  - With detailed reason of invitation, representative of inviting company or organization
  - With Official Stamp ealed and also Stamp or autograph representative of inviting company or organizations sealed. 
- The business certificate of the company(사업자등록증 혹은 사업자등록증명원)
- Original gurantee letter by the invitor if you have one (Please see the attached file from the link: https://overseas.mofa.go.kr/np-ko/brd/m_1730/view.do?seq=1344334&srchFr=&srchTo=&srchWord=&srchTp=&multi_itm_seq=0&itm_seq_1=0&itm_seq_2=0&company_cd=&company_nm=&page=1)

## Notes

- The embassy do NOT accept any visa applications by E-mail.
- Please be informed that it takes about 1 working week for the result.
- If you are not a Israeli National, you have to hold residential Visa in Israel to apply for Visa through us. (Make a photocopy of your Israel visa and submit it with your application)
- The documents validity for visa application is 3 months only. We do not accept any documents which were issued more than 3 months before the date of your visa application has been accepted.
- We will be holding your passport during the visa process.
- The cancellation of visa will take 1 working day.
- We do NOT provide ANY TRACKING SERVICE(You can check your visa progress by online, please click the website https://visa.go.kr/openPage.do?MENU_ID=10301&LANG_TYPE=EN), after you submit your application. It may cause delay on your application if you try to follow up your visa process.
- If you plan to stay in Korea more than 90 days, you have to have a specific reason with official documents to stay in Korea to obtain a proper long-term Visa.
- Please type or print clearly when you fill out the Visa application form AND DO NOT FORGET TO SIGN AT THE BOTTOM.
- Need at least 6 months validity on the passport to apply for a visa
- Please email your preference time to israel@mofa.go.kr for booking your visa interview before ahead of time. 
- As most visas are valid for 3 months from the issue date, please apply for visa no more than 2 months in advance from the planned date of entry into South Korea.

## Office hours of the Embassy in Israel 
09:30-12:00, Mondays ~ Thursdays

 
